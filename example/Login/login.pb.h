// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: login.proto

#ifndef PROTOBUF_login_2eproto__INCLUDED
#define PROTOBUF_login_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 2005000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 2005000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/service.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace sails {

// Internal implementation detail -- do not call these.
void  protobuf_AddDesc_login_2eproto();
void protobuf_AssignDesc_login_2eproto();
void protobuf_ShutdownFile_login_2eproto();

class LoginRequest;
class LoginResponse;
class LogoutRequest;
class LogoutResponse;

enum LoginResponse_ResultCode {
  LoginResponse_ResultCode_LOGIN_SUCCESS = 0,
  LoginResponse_ResultCode_LOGIN_CHECK_FAIL = 1,
  LoginResponse_ResultCode_LOGIN_GET_ROOM_FAIL = 2,
  LoginResponse_ResultCode_LOGIN_ROOM_FULL = 3,
  LoginResponse_ResultCode_LOGIN_ADD_SESSION_FAIL = 4,
  LoginResponse_ResultCode_LOGIN_OTHER_ERR = 10
};
bool LoginResponse_ResultCode_IsValid(int value);
const LoginResponse_ResultCode LoginResponse_ResultCode_ResultCode_MIN = LoginResponse_ResultCode_LOGIN_SUCCESS;
const LoginResponse_ResultCode LoginResponse_ResultCode_ResultCode_MAX = LoginResponse_ResultCode_LOGIN_OTHER_ERR;
const int LoginResponse_ResultCode_ResultCode_ARRAYSIZE = LoginResponse_ResultCode_ResultCode_MAX + 1;

const ::google::protobuf::EnumDescriptor* LoginResponse_ResultCode_descriptor();
inline const ::std::string& LoginResponse_ResultCode_Name(LoginResponse_ResultCode value) {
  return ::google::protobuf::internal::NameOfEnum(
    LoginResponse_ResultCode_descriptor(), value);
}
inline bool LoginResponse_ResultCode_Parse(
    const ::std::string& name, LoginResponse_ResultCode* value) {
  return ::google::protobuf::internal::ParseNamedEnum<LoginResponse_ResultCode>(
    LoginResponse_ResultCode_descriptor(), name, value);
}
enum LogoutResponse_ResultCode {
  LogoutResponse_ResultCode_LOGOUT_SUCCESS = 0,
  LogoutResponse_ResultCode_LOGOUT_ERR = 1
};
bool LogoutResponse_ResultCode_IsValid(int value);
const LogoutResponse_ResultCode LogoutResponse_ResultCode_ResultCode_MIN = LogoutResponse_ResultCode_LOGOUT_SUCCESS;
const LogoutResponse_ResultCode LogoutResponse_ResultCode_ResultCode_MAX = LogoutResponse_ResultCode_LOGOUT_ERR;
const int LogoutResponse_ResultCode_ResultCode_ARRAYSIZE = LogoutResponse_ResultCode_ResultCode_MAX + 1;

const ::google::protobuf::EnumDescriptor* LogoutResponse_ResultCode_descriptor();
inline const ::std::string& LogoutResponse_ResultCode_Name(LogoutResponse_ResultCode value) {
  return ::google::protobuf::internal::NameOfEnum(
    LogoutResponse_ResultCode_descriptor(), value);
}
inline bool LogoutResponse_ResultCode_Parse(
    const ::std::string& name, LogoutResponse_ResultCode* value) {
  return ::google::protobuf::internal::ParseNamedEnum<LogoutResponse_ResultCode>(
    LogoutResponse_ResultCode_descriptor(), name, value);
}
// ===================================================================

class LoginRequest : public ::google::protobuf::Message {
 public:
  LoginRequest();
  virtual ~LoginRequest();

  LoginRequest(const LoginRequest& from);

  inline LoginRequest& operator=(const LoginRequest& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const LoginRequest& default_instance();

  void Swap(LoginRequest* other);

  // implements Message ----------------------------------------------

  LoginRequest* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const LoginRequest& from);
  void MergeFrom(const LoginRequest& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required string username = 1;
  inline bool has_username() const;
  inline void clear_username();
  static const int kUsernameFieldNumber = 1;
  inline const ::std::string& username() const;
  inline void set_username(const ::std::string& value);
  inline void set_username(const char* value);
  inline void set_username(const char* value, size_t size);
  inline ::std::string* mutable_username();
  inline ::std::string* release_username();
  inline void set_allocated_username(::std::string* username);

  // required string ticket = 2;
  inline bool has_ticket() const;
  inline void clear_ticket();
  static const int kTicketFieldNumber = 2;
  inline const ::std::string& ticket() const;
  inline void set_ticket(const ::std::string& value);
  inline void set_ticket(const char* value);
  inline void set_ticket(const char* value, size_t size);
  inline ::std::string* mutable_ticket();
  inline ::std::string* release_ticket();
  inline void set_allocated_ticket(::std::string* ticket);

  // required int32 roomid = 3;
  inline bool has_roomid() const;
  inline void clear_roomid();
  static const int kRoomidFieldNumber = 3;
  inline ::google::protobuf::int32 roomid() const;
  inline void set_roomid(::google::protobuf::int32 value);

  // @@protoc_insertion_point(class_scope:sails.LoginRequest)
 private:
  inline void set_has_username();
  inline void clear_has_username();
  inline void set_has_ticket();
  inline void clear_has_ticket();
  inline void set_has_roomid();
  inline void clear_has_roomid();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::std::string* username_;
  ::std::string* ticket_;
  ::google::protobuf::int32 roomid_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(3 + 31) / 32];

  friend void  protobuf_AddDesc_login_2eproto();
  friend void protobuf_AssignDesc_login_2eproto();
  friend void protobuf_ShutdownFile_login_2eproto();

  void InitAsDefaultInstance();
  static LoginRequest* default_instance_;
};
// -------------------------------------------------------------------

class LoginResponse : public ::google::protobuf::Message {
 public:
  LoginResponse();
  virtual ~LoginResponse();

  LoginResponse(const LoginResponse& from);

  inline LoginResponse& operator=(const LoginResponse& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const LoginResponse& default_instance();

  void Swap(LoginResponse* other);

  // implements Message ----------------------------------------------

  LoginResponse* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const LoginResponse& from);
  void MergeFrom(const LoginResponse& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef LoginResponse_ResultCode ResultCode;
  static const ResultCode LOGIN_SUCCESS = LoginResponse_ResultCode_LOGIN_SUCCESS;
  static const ResultCode LOGIN_CHECK_FAIL = LoginResponse_ResultCode_LOGIN_CHECK_FAIL;
  static const ResultCode LOGIN_GET_ROOM_FAIL = LoginResponse_ResultCode_LOGIN_GET_ROOM_FAIL;
  static const ResultCode LOGIN_ROOM_FULL = LoginResponse_ResultCode_LOGIN_ROOM_FULL;
  static const ResultCode LOGIN_ADD_SESSION_FAIL = LoginResponse_ResultCode_LOGIN_ADD_SESSION_FAIL;
  static const ResultCode LOGIN_OTHER_ERR = LoginResponse_ResultCode_LOGIN_OTHER_ERR;
  static inline bool ResultCode_IsValid(int value) {
    return LoginResponse_ResultCode_IsValid(value);
  }
  static const ResultCode ResultCode_MIN =
    LoginResponse_ResultCode_ResultCode_MIN;
  static const ResultCode ResultCode_MAX =
    LoginResponse_ResultCode_ResultCode_MAX;
  static const int ResultCode_ARRAYSIZE =
    LoginResponse_ResultCode_ResultCode_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  ResultCode_descriptor() {
    return LoginResponse_ResultCode_descriptor();
  }
  static inline const ::std::string& ResultCode_Name(ResultCode value) {
    return LoginResponse_ResultCode_Name(value);
  }
  static inline bool ResultCode_Parse(const ::std::string& name,
      ResultCode* value) {
    return LoginResponse_ResultCode_Parse(name, value);
  }

  // accessors -------------------------------------------------------

  // required .sails.LoginResponse.ResultCode code = 1;
  inline bool has_code() const;
  inline void clear_code();
  static const int kCodeFieldNumber = 1;
  inline ::sails::LoginResponse_ResultCode code() const;
  inline void set_code(::sails::LoginResponse_ResultCode value);

  // optional int32 roomid = 2;
  inline bool has_roomid() const;
  inline void clear_roomid();
  static const int kRoomidFieldNumber = 2;
  inline ::google::protobuf::int32 roomid() const;
  inline void set_roomid(::google::protobuf::int32 value);

  // optional string ip = 3;
  inline bool has_ip() const;
  inline void clear_ip();
  static const int kIpFieldNumber = 3;
  inline const ::std::string& ip() const;
  inline void set_ip(const ::std::string& value);
  inline void set_ip(const char* value);
  inline void set_ip(const char* value, size_t size);
  inline ::std::string* mutable_ip();
  inline ::std::string* release_ip();
  inline void set_allocated_ip(::std::string* ip);

  // optional int32 port = 4;
  inline bool has_port() const;
  inline void clear_port();
  static const int kPortFieldNumber = 4;
  inline ::google::protobuf::int32 port() const;
  inline void set_port(::google::protobuf::int32 value);

  // optional string session = 5;
  inline bool has_session() const;
  inline void clear_session();
  static const int kSessionFieldNumber = 5;
  inline const ::std::string& session() const;
  inline void set_session(const ::std::string& value);
  inline void set_session(const char* value);
  inline void set_session(const char* value, size_t size);
  inline ::std::string* mutable_session();
  inline ::std::string* release_session();
  inline void set_allocated_session(::std::string* session);

  // @@protoc_insertion_point(class_scope:sails.LoginResponse)
 private:
  inline void set_has_code();
  inline void clear_has_code();
  inline void set_has_roomid();
  inline void clear_has_roomid();
  inline void set_has_ip();
  inline void clear_has_ip();
  inline void set_has_port();
  inline void clear_has_port();
  inline void set_has_session();
  inline void clear_has_session();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  int code_;
  ::google::protobuf::int32 roomid_;
  ::std::string* ip_;
  ::std::string* session_;
  ::google::protobuf::int32 port_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(5 + 31) / 32];

  friend void  protobuf_AddDesc_login_2eproto();
  friend void protobuf_AssignDesc_login_2eproto();
  friend void protobuf_ShutdownFile_login_2eproto();

  void InitAsDefaultInstance();
  static LoginResponse* default_instance_;
};
// -------------------------------------------------------------------

class LogoutRequest : public ::google::protobuf::Message {
 public:
  LogoutRequest();
  virtual ~LogoutRequest();

  LogoutRequest(const LogoutRequest& from);

  inline LogoutRequest& operator=(const LogoutRequest& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const LogoutRequest& default_instance();

  void Swap(LogoutRequest* other);

  // implements Message ----------------------------------------------

  LogoutRequest* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const LogoutRequest& from);
  void MergeFrom(const LogoutRequest& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required string session = 1;
  inline bool has_session() const;
  inline void clear_session();
  static const int kSessionFieldNumber = 1;
  inline const ::std::string& session() const;
  inline void set_session(const ::std::string& value);
  inline void set_session(const char* value);
  inline void set_session(const char* value, size_t size);
  inline ::std::string* mutable_session();
  inline ::std::string* release_session();
  inline void set_allocated_session(::std::string* session);

  // @@protoc_insertion_point(class_scope:sails.LogoutRequest)
 private:
  inline void set_has_session();
  inline void clear_has_session();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  ::std::string* session_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(1 + 31) / 32];

  friend void  protobuf_AddDesc_login_2eproto();
  friend void protobuf_AssignDesc_login_2eproto();
  friend void protobuf_ShutdownFile_login_2eproto();

  void InitAsDefaultInstance();
  static LogoutRequest* default_instance_;
};
// -------------------------------------------------------------------

class LogoutResponse : public ::google::protobuf::Message {
 public:
  LogoutResponse();
  virtual ~LogoutResponse();

  LogoutResponse(const LogoutResponse& from);

  inline LogoutResponse& operator=(const LogoutResponse& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _unknown_fields_;
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return &_unknown_fields_;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const LogoutResponse& default_instance();

  void Swap(LogoutResponse* other);

  // implements Message ----------------------------------------------

  LogoutResponse* New() const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const LogoutResponse& from);
  void MergeFrom(const LogoutResponse& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef LogoutResponse_ResultCode ResultCode;
  static const ResultCode LOGOUT_SUCCESS = LogoutResponse_ResultCode_LOGOUT_SUCCESS;
  static const ResultCode LOGOUT_ERR = LogoutResponse_ResultCode_LOGOUT_ERR;
  static inline bool ResultCode_IsValid(int value) {
    return LogoutResponse_ResultCode_IsValid(value);
  }
  static const ResultCode ResultCode_MIN =
    LogoutResponse_ResultCode_ResultCode_MIN;
  static const ResultCode ResultCode_MAX =
    LogoutResponse_ResultCode_ResultCode_MAX;
  static const int ResultCode_ARRAYSIZE =
    LogoutResponse_ResultCode_ResultCode_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  ResultCode_descriptor() {
    return LogoutResponse_ResultCode_descriptor();
  }
  static inline const ::std::string& ResultCode_Name(ResultCode value) {
    return LogoutResponse_ResultCode_Name(value);
  }
  static inline bool ResultCode_Parse(const ::std::string& name,
      ResultCode* value) {
    return LogoutResponse_ResultCode_Parse(name, value);
  }

  // accessors -------------------------------------------------------

  // required .sails.LogoutResponse.ResultCode code = 1;
  inline bool has_code() const;
  inline void clear_code();
  static const int kCodeFieldNumber = 1;
  inline ::sails::LogoutResponse_ResultCode code() const;
  inline void set_code(::sails::LogoutResponse_ResultCode value);

  // @@protoc_insertion_point(class_scope:sails.LogoutResponse)
 private:
  inline void set_has_code();
  inline void clear_has_code();

  ::google::protobuf::UnknownFieldSet _unknown_fields_;

  int code_;

  mutable int _cached_size_;
  ::google::protobuf::uint32 _has_bits_[(1 + 31) / 32];

  friend void  protobuf_AddDesc_login_2eproto();
  friend void protobuf_AssignDesc_login_2eproto();
  friend void protobuf_ShutdownFile_login_2eproto();

  void InitAsDefaultInstance();
  static LogoutResponse* default_instance_;
};
// ===================================================================

class LoginService_Stub;

class LoginService : public ::google::protobuf::Service {
 protected:
  // This class should be treated as an abstract interface.
  inline LoginService() {};
 public:
  virtual ~LoginService();

  typedef LoginService_Stub Stub;

  static const ::google::protobuf::ServiceDescriptor* descriptor();

  virtual void login(::google::protobuf::RpcController* controller,
                       const ::sails::LoginRequest* request,
                       ::sails::LoginResponse* response,
                       ::google::protobuf::Closure* done);
  virtual void logout(::google::protobuf::RpcController* controller,
                       const ::sails::LogoutRequest* request,
                       ::sails::LogoutResponse* response,
                       ::google::protobuf::Closure* done);

  // implements Service ----------------------------------------------

  const ::google::protobuf::ServiceDescriptor* GetDescriptor();
  void CallMethod(const ::google::protobuf::MethodDescriptor* method,
                  ::google::protobuf::RpcController* controller,
                  const ::google::protobuf::Message* request,
                  ::google::protobuf::Message* response,
                  ::google::protobuf::Closure* done);
  const ::google::protobuf::Message& GetRequestPrototype(
    const ::google::protobuf::MethodDescriptor* method) const;
  const ::google::protobuf::Message& GetResponsePrototype(
    const ::google::protobuf::MethodDescriptor* method) const;

 private:
  GOOGLE_DISALLOW_EVIL_CONSTRUCTORS(LoginService);
};

class LoginService_Stub : public LoginService {
 public:
  LoginService_Stub(::google::protobuf::RpcChannel* channel);
  LoginService_Stub(::google::protobuf::RpcChannel* channel,
                   ::google::protobuf::Service::ChannelOwnership ownership);
  ~LoginService_Stub();

  inline ::google::protobuf::RpcChannel* channel() { return channel_; }

  // implements LoginService ------------------------------------------

  void login(::google::protobuf::RpcController* controller,
                       const ::sails::LoginRequest* request,
                       ::sails::LoginResponse* response,
                       ::google::protobuf::Closure* done);
  void logout(::google::protobuf::RpcController* controller,
                       const ::sails::LogoutRequest* request,
                       ::sails::LogoutResponse* response,
                       ::google::protobuf::Closure* done);
 private:
  ::google::protobuf::RpcChannel* channel_;
  bool owns_channel_;
  GOOGLE_DISALLOW_EVIL_CONSTRUCTORS(LoginService_Stub);
};


// ===================================================================


// ===================================================================

// LoginRequest

// required string username = 1;
inline bool LoginRequest::has_username() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void LoginRequest::set_has_username() {
  _has_bits_[0] |= 0x00000001u;
}
inline void LoginRequest::clear_has_username() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void LoginRequest::clear_username() {
  if (username_ != &::google::protobuf::internal::kEmptyString) {
    username_->clear();
  }
  clear_has_username();
}
inline const ::std::string& LoginRequest::username() const {
  return *username_;
}
inline void LoginRequest::set_username(const ::std::string& value) {
  set_has_username();
  if (username_ == &::google::protobuf::internal::kEmptyString) {
    username_ = new ::std::string;
  }
  username_->assign(value);
}
inline void LoginRequest::set_username(const char* value) {
  set_has_username();
  if (username_ == &::google::protobuf::internal::kEmptyString) {
    username_ = new ::std::string;
  }
  username_->assign(value);
}
inline void LoginRequest::set_username(const char* value, size_t size) {
  set_has_username();
  if (username_ == &::google::protobuf::internal::kEmptyString) {
    username_ = new ::std::string;
  }
  username_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* LoginRequest::mutable_username() {
  set_has_username();
  if (username_ == &::google::protobuf::internal::kEmptyString) {
    username_ = new ::std::string;
  }
  return username_;
}
inline ::std::string* LoginRequest::release_username() {
  clear_has_username();
  if (username_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = username_;
    username_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void LoginRequest::set_allocated_username(::std::string* username) {
  if (username_ != &::google::protobuf::internal::kEmptyString) {
    delete username_;
  }
  if (username) {
    set_has_username();
    username_ = username;
  } else {
    clear_has_username();
    username_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// required string ticket = 2;
inline bool LoginRequest::has_ticket() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void LoginRequest::set_has_ticket() {
  _has_bits_[0] |= 0x00000002u;
}
inline void LoginRequest::clear_has_ticket() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void LoginRequest::clear_ticket() {
  if (ticket_ != &::google::protobuf::internal::kEmptyString) {
    ticket_->clear();
  }
  clear_has_ticket();
}
inline const ::std::string& LoginRequest::ticket() const {
  return *ticket_;
}
inline void LoginRequest::set_ticket(const ::std::string& value) {
  set_has_ticket();
  if (ticket_ == &::google::protobuf::internal::kEmptyString) {
    ticket_ = new ::std::string;
  }
  ticket_->assign(value);
}
inline void LoginRequest::set_ticket(const char* value) {
  set_has_ticket();
  if (ticket_ == &::google::protobuf::internal::kEmptyString) {
    ticket_ = new ::std::string;
  }
  ticket_->assign(value);
}
inline void LoginRequest::set_ticket(const char* value, size_t size) {
  set_has_ticket();
  if (ticket_ == &::google::protobuf::internal::kEmptyString) {
    ticket_ = new ::std::string;
  }
  ticket_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* LoginRequest::mutable_ticket() {
  set_has_ticket();
  if (ticket_ == &::google::protobuf::internal::kEmptyString) {
    ticket_ = new ::std::string;
  }
  return ticket_;
}
inline ::std::string* LoginRequest::release_ticket() {
  clear_has_ticket();
  if (ticket_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = ticket_;
    ticket_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void LoginRequest::set_allocated_ticket(::std::string* ticket) {
  if (ticket_ != &::google::protobuf::internal::kEmptyString) {
    delete ticket_;
  }
  if (ticket) {
    set_has_ticket();
    ticket_ = ticket;
  } else {
    clear_has_ticket();
    ticket_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// required int32 roomid = 3;
inline bool LoginRequest::has_roomid() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void LoginRequest::set_has_roomid() {
  _has_bits_[0] |= 0x00000004u;
}
inline void LoginRequest::clear_has_roomid() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void LoginRequest::clear_roomid() {
  roomid_ = 0;
  clear_has_roomid();
}
inline ::google::protobuf::int32 LoginRequest::roomid() const {
  return roomid_;
}
inline void LoginRequest::set_roomid(::google::protobuf::int32 value) {
  set_has_roomid();
  roomid_ = value;
}

// -------------------------------------------------------------------

// LoginResponse

// required .sails.LoginResponse.ResultCode code = 1;
inline bool LoginResponse::has_code() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void LoginResponse::set_has_code() {
  _has_bits_[0] |= 0x00000001u;
}
inline void LoginResponse::clear_has_code() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void LoginResponse::clear_code() {
  code_ = 0;
  clear_has_code();
}
inline ::sails::LoginResponse_ResultCode LoginResponse::code() const {
  return static_cast< ::sails::LoginResponse_ResultCode >(code_);
}
inline void LoginResponse::set_code(::sails::LoginResponse_ResultCode value) {
  assert(::sails::LoginResponse_ResultCode_IsValid(value));
  set_has_code();
  code_ = value;
}

// optional int32 roomid = 2;
inline bool LoginResponse::has_roomid() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void LoginResponse::set_has_roomid() {
  _has_bits_[0] |= 0x00000002u;
}
inline void LoginResponse::clear_has_roomid() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void LoginResponse::clear_roomid() {
  roomid_ = 0;
  clear_has_roomid();
}
inline ::google::protobuf::int32 LoginResponse::roomid() const {
  return roomid_;
}
inline void LoginResponse::set_roomid(::google::protobuf::int32 value) {
  set_has_roomid();
  roomid_ = value;
}

// optional string ip = 3;
inline bool LoginResponse::has_ip() const {
  return (_has_bits_[0] & 0x00000004u) != 0;
}
inline void LoginResponse::set_has_ip() {
  _has_bits_[0] |= 0x00000004u;
}
inline void LoginResponse::clear_has_ip() {
  _has_bits_[0] &= ~0x00000004u;
}
inline void LoginResponse::clear_ip() {
  if (ip_ != &::google::protobuf::internal::kEmptyString) {
    ip_->clear();
  }
  clear_has_ip();
}
inline const ::std::string& LoginResponse::ip() const {
  return *ip_;
}
inline void LoginResponse::set_ip(const ::std::string& value) {
  set_has_ip();
  if (ip_ == &::google::protobuf::internal::kEmptyString) {
    ip_ = new ::std::string;
  }
  ip_->assign(value);
}
inline void LoginResponse::set_ip(const char* value) {
  set_has_ip();
  if (ip_ == &::google::protobuf::internal::kEmptyString) {
    ip_ = new ::std::string;
  }
  ip_->assign(value);
}
inline void LoginResponse::set_ip(const char* value, size_t size) {
  set_has_ip();
  if (ip_ == &::google::protobuf::internal::kEmptyString) {
    ip_ = new ::std::string;
  }
  ip_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* LoginResponse::mutable_ip() {
  set_has_ip();
  if (ip_ == &::google::protobuf::internal::kEmptyString) {
    ip_ = new ::std::string;
  }
  return ip_;
}
inline ::std::string* LoginResponse::release_ip() {
  clear_has_ip();
  if (ip_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = ip_;
    ip_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void LoginResponse::set_allocated_ip(::std::string* ip) {
  if (ip_ != &::google::protobuf::internal::kEmptyString) {
    delete ip_;
  }
  if (ip) {
    set_has_ip();
    ip_ = ip;
  } else {
    clear_has_ip();
    ip_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// optional int32 port = 4;
inline bool LoginResponse::has_port() const {
  return (_has_bits_[0] & 0x00000008u) != 0;
}
inline void LoginResponse::set_has_port() {
  _has_bits_[0] |= 0x00000008u;
}
inline void LoginResponse::clear_has_port() {
  _has_bits_[0] &= ~0x00000008u;
}
inline void LoginResponse::clear_port() {
  port_ = 0;
  clear_has_port();
}
inline ::google::protobuf::int32 LoginResponse::port() const {
  return port_;
}
inline void LoginResponse::set_port(::google::protobuf::int32 value) {
  set_has_port();
  port_ = value;
}

// optional string session = 5;
inline bool LoginResponse::has_session() const {
  return (_has_bits_[0] & 0x00000010u) != 0;
}
inline void LoginResponse::set_has_session() {
  _has_bits_[0] |= 0x00000010u;
}
inline void LoginResponse::clear_has_session() {
  _has_bits_[0] &= ~0x00000010u;
}
inline void LoginResponse::clear_session() {
  if (session_ != &::google::protobuf::internal::kEmptyString) {
    session_->clear();
  }
  clear_has_session();
}
inline const ::std::string& LoginResponse::session() const {
  return *session_;
}
inline void LoginResponse::set_session(const ::std::string& value) {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  session_->assign(value);
}
inline void LoginResponse::set_session(const char* value) {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  session_->assign(value);
}
inline void LoginResponse::set_session(const char* value, size_t size) {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  session_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* LoginResponse::mutable_session() {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  return session_;
}
inline ::std::string* LoginResponse::release_session() {
  clear_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = session_;
    session_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void LoginResponse::set_allocated_session(::std::string* session) {
  if (session_ != &::google::protobuf::internal::kEmptyString) {
    delete session_;
  }
  if (session) {
    set_has_session();
    session_ = session;
  } else {
    clear_has_session();
    session_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// -------------------------------------------------------------------

// LogoutRequest

// required string session = 1;
inline bool LogoutRequest::has_session() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void LogoutRequest::set_has_session() {
  _has_bits_[0] |= 0x00000001u;
}
inline void LogoutRequest::clear_has_session() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void LogoutRequest::clear_session() {
  if (session_ != &::google::protobuf::internal::kEmptyString) {
    session_->clear();
  }
  clear_has_session();
}
inline const ::std::string& LogoutRequest::session() const {
  return *session_;
}
inline void LogoutRequest::set_session(const ::std::string& value) {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  session_->assign(value);
}
inline void LogoutRequest::set_session(const char* value) {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  session_->assign(value);
}
inline void LogoutRequest::set_session(const char* value, size_t size) {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  session_->assign(reinterpret_cast<const char*>(value), size);
}
inline ::std::string* LogoutRequest::mutable_session() {
  set_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    session_ = new ::std::string;
  }
  return session_;
}
inline ::std::string* LogoutRequest::release_session() {
  clear_has_session();
  if (session_ == &::google::protobuf::internal::kEmptyString) {
    return NULL;
  } else {
    ::std::string* temp = session_;
    session_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
    return temp;
  }
}
inline void LogoutRequest::set_allocated_session(::std::string* session) {
  if (session_ != &::google::protobuf::internal::kEmptyString) {
    delete session_;
  }
  if (session) {
    set_has_session();
    session_ = session;
  } else {
    clear_has_session();
    session_ = const_cast< ::std::string*>(&::google::protobuf::internal::kEmptyString);
  }
}

// -------------------------------------------------------------------

// LogoutResponse

// required .sails.LogoutResponse.ResultCode code = 1;
inline bool LogoutResponse::has_code() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void LogoutResponse::set_has_code() {
  _has_bits_[0] |= 0x00000001u;
}
inline void LogoutResponse::clear_has_code() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void LogoutResponse::clear_code() {
  code_ = 0;
  clear_has_code();
}
inline ::sails::LogoutResponse_ResultCode LogoutResponse::code() const {
  return static_cast< ::sails::LogoutResponse_ResultCode >(code_);
}
inline void LogoutResponse::set_code(::sails::LogoutResponse_ResultCode value) {
  assert(::sails::LogoutResponse_ResultCode_IsValid(value));
  set_has_code();
  code_ = value;
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace sails

#ifndef SWIG
namespace google {
namespace protobuf {

template <>
inline const EnumDescriptor* GetEnumDescriptor< ::sails::LoginResponse_ResultCode>() {
  return ::sails::LoginResponse_ResultCode_descriptor();
}
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::sails::LogoutResponse_ResultCode>() {
  return ::sails::LogoutResponse_ResultCode_descriptor();
}

}  // namespace google
}  // namespace protobuf
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_login_2eproto__INCLUDED
